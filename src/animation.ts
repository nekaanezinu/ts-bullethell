import { Game } from "./game";

// TODO: absolute mess

interface LoadableAnimation {
	baseKey: string;
	basePath: string;
	frameCount: number;
	offsetX: number;
	offsetY: number;
	baseDamage: number;
	invertable?: boolean;
}

class Frame {
	key: string;
	path: string;

	constructor(key: string, path: string) {
		this.key = key;
		this.path = path;
	}
}

class InnerAnimation {
	baseKey: string;
	frames: Frame[] = [];
	sprite: Phaser.Types.Physics.Arcade.SpriteWithDynamicBody | undefined;
	offsetX: number;
	offsetY: number;
	invertable: boolean = false;
	inverted: boolean = false;
	baseDamage: number;
	active: boolean = false;

	constructor(
		context: Game,
		baseKey: string,
		basePath: string,
		frameCount: number,
		offsetX: number,
		offsetY: number,
		baseDamage: number,
		invertable: boolean = false
	) {
		this.baseKey = baseKey;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.baseDamage = baseDamage;
		this.invertable = invertable;

		for (let index = 1; index <= frameCount; index++) {
			let path = basePath + "/" + index + ".png";
			this.frames.push(new Frame(baseKey + index, path));
		}
	}

	play() {
		this.sprite!.play(this.baseKey);
	}
}

class Animations {
	animations: { [key: string]: InnerAnimation } = {};
	context: Game;

	constructor(context: Game, anims: LoadableAnimation[]) {
		this.context = context;
		anims.forEach((anim) => {
			this.animations[anim.baseKey] = new InnerAnimation(
				context,
				anim.baseKey,
				anim.basePath,
				anim.frameCount,
				anim.offsetX,
				anim.offsetY,
				anim.baseDamage,
				anim.invertable
			);
		});
	}

	load(context: Game): void {
		Object.keys(this.animations).forEach((key) => {
			this.animations[key].frames.forEach((frame) => {
				context.load.image(frame.key, frame.path);
			});
		});
	}

	createAnimations(context: Game): void {
		Object.keys(this.animations).forEach((key) => {
			let frameNames = this.animations[key].frames.map((frame) => {
				return {
					key: frame.key,
				};
			});

			context.anims.create({
				key: key,
				frames: frameNames,
				frameRate: 25,
			});

			this.animations[key].sprite = context.physics.add.sprite(
				400 + this.animations[key].offsetX,
				300 + this.animations[key].offsetY,
				this.animations[key].frames[0].key
			);
			this.animations[key].sprite!.setInteractive();

			this.playAnimation(key);
		});
	}

	playAnimation(key: string): void {
		this.animations[key].sprite!.setVisible(true);
		this.animations[key].sprite!.play(this.animations[key].baseKey);
		this.animations[key].active = true;
		this.context.time.delayedCall(300, () => {
			this.animations[key].sprite!.setVisible(false);
			this.animations[key].active = false;
			this.context.time.delayedCall(1000, () =>
				this.playAnimation.call(this, key)
			);
		});
	}
}

export { Animations, InnerAnimation };

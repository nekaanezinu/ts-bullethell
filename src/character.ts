class Character {
	base_image: string;

	constructor(base_image: string) {
		this.base_image = base_image;
	}
}

const characters = {
	warrior: new Character("warrior_base"),
};

export { Character, characters };

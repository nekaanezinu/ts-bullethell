import * as Phaser from "phaser";

interface Config {
	width: number;
	height: number;
	backgroundColor: string;
}

const innerConfig: Config = {
	width: 800,
	height: 600,
	backgroundColor: "#2d2d2d",
};

const config: Phaser.Types.Core.GameConfig = {
	type: Phaser.AUTO,
	width: innerConfig.width,
	height: innerConfig.height,
	backgroundColor: innerConfig.backgroundColor,
	parent: document.getElementById("gameContainer")!,
	physics: {
		default: "arcade",
		arcade: {
			debug: true,
		},
	},
};

export { config, innerConfig };

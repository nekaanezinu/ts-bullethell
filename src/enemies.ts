import { InnerAnimation } from "./animation";
import { Enemy } from "./enemy";
import { Game } from "./game";
import { HealthEvents } from "./health";
import { Player } from "./player";

class Enemies {
  list: Enemy[] = [];
  spriteGroup: Phaser.Physics.Arcade.Group;
  collider: Phaser.Physics.Arcade.Collider;
  context: Game;

  constructor(context: Game) {
    this.context = context;
    this.spriteGroup = context.physics.add.group();
    this.collider = context.physics.add.collider(
      this.spriteGroup,
      this.spriteGroup
    );
  }

  add(enemy: Enemy): void {
    this.list.push(enemy);
    this.spriteGroup.add(enemy.image!);
    Object.keys(this.context.animations!.animations).forEach((key) => {
      this.refreshOverlaps(this.context.animations?.animations[key]!);
    });
  }

  remove(enemy: Enemy): void {
    this.list = this.list.filter((e) => e !== enemy);
    this.spriteGroup.remove(enemy.image!);
    Object.keys(this.context.animations!.animations).forEach((key) => {
      this.refreshOverlaps(this.context.animations?.animations[key]!);
    });
  }

  move(player: Player): void {
    this.list.forEach((enemy) => enemy.move(player));
  }

  refreshOverlaps(animation: InnerAnimation): void {
    console.log("here");
    this.list.forEach((enemy) => {
      this.context.physics.add.overlap(
        this.spriteGroup,
        animation.sprite!,
        () => {
          enemy.hit(animation);
        },
        undefined,
        this
      );
    });
  }
}

export { Enemies };

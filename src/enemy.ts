import { InnerAnimation } from "./animation";
import { Game } from "./game";
import { HealthEvents, Health } from "./health";
import { Player } from "./player";
import { Movable } from "./utils/movable";

class Enemy {
  name: string;
  base_image: string;
  image: Phaser.Types.Physics.Arcade.ImageWithDynamicBody | undefined;
  movable: Movable | undefined;
  health: Health;
  context: Game;
  iframes: number = 0;
  private baseAttack: number = 1;

  constructor(context: Game, name: string, base_image: string, health: number) {
    this.context = context;
    this.name = name;
    this.base_image = base_image;
    this.health = new Health(health);
    // this.health.addEvent(HealthEvents.CHANGE, () => {
    //   console.log(`zombie health: ${this.health.current}`);
    // });
  }

  destroy(): void {
    this.context.enemies!.remove(this);
    this.context.physics.add.image(this.image!.x, this.image!.y, "xp_1");

    if (this.image) {
      this.image.destroy();
    }
  }

  draw(
    context: Game,
    x: number,
    y: number
  ): Phaser.Types.Physics.Arcade.ImageWithDynamicBody {
    this.image = context.physics.add.image(x, y, this.base_image);
    this.movable = new Movable(this.image!);

    this.image.setInteractive(); // maybe not needed?
    this.context.enemies!.add(this);

    return this.image!;
  }

  public get damage(): number {
    // TODO: multipliers
    return this.baseAttack;
  }

  hit(animation: InnerAnimation): void {
    if (!animation.active) {
      return;
    }
    if (this.iframes > 0) {
      return;
    }

    this.iframes = 30;

    this.knockback(this.context.player!);
    this.takeDamage(animation.baseDamage);
    this.health.addEvent(HealthEvents.CHANGE, () => {
      console.log(`enemy health: ${this.health.current}`);
    });
    this.health.addEvent(HealthEvents.DEATH, () => {
      console.log(`${this.name} death`);
      this.destroy();
    });
  }

  knockback(player: Player) {
    this.movable!.knockback();
  }

  move(player: Player): void {
    this.moveToPlayer(player);
    this.moveKnockback(player);
  }

  moveToPlayer(player: Player) {
    this.movable!.moveTowardsMovable(player.movable!);
  }

  moveKnockback(player: Player): void {
    this.movable!.moveAwayFrom(player.movable!);
  }

  takeDamage(damage: number): void {
    this.health.takeDamage(damage);
  }
}

const enemies = {
  zombie: (context: Game) => {
    return new Enemy(context, "zombie", "zombie_base", 25);
  },
};

export { Enemy, enemies };

import * as Phaser from "phaser";
import { Player } from "./player";
import { characters } from "./character";
import { enemies } from "./enemy";
import { Animations } from "./animation";
import { Hud } from "./hud/main";
import { Enemies } from "./enemies";
import { Spawner } from "./spawner";
import { innerConfig } from "./config";
import { DebugConsole } from "./utils/console";
import { Debugger } from "./utils/debugger";

interface Keys {
	W: Phaser.Input.Keyboard.Key;
	A: Phaser.Input.Keyboard.Key;
	S: Phaser.Input.Keyboard.Key;
	D: Phaser.Input.Keyboard.Key;
}

class Game extends Phaser.Scene {
	keys: Keys | undefined;
	animations: Animations | undefined;
	hud: Hud | undefined;
	player: Player | undefined;
	enemies: Enemies | undefined;
	spawner: Spawner | undefined;
	console: DebugConsole | undefined;

	preload(): void {
		// TODO: move this to an "assets"? class
		this.load.image("warrior_base", "assets/characters/warrior/base.png");
		this.load.image("zombie_base", "assets/enemies/zombie/base.png");
		this.load.image("xp_1", "assets/xp_1.png");
		this.animations = new Animations(this, [
			// {
			//   baseKey: "blue_slash",
			//   basePath: "assets/attacks/blue_slash",
			//   frameCount: 8,
			//   offsetX: 40,
			//   offsetY: 0,
			//   invertable: true,
			// },
			{
				baseKey: "yellow_slash",
				basePath: "assets/attacks/yellow_slash",
				frameCount: 3,
				offsetX: 50,
				offsetY: 0,
				baseDamage: 5,
				invertable: true,
			},
		]);
		this.animations.load(this);
	}

	create(): void {
		this.hud = new Hud(this);
		this.player = new Player(characters.warrior, this.hud!);
		this.animations?.createAnimations(this);
		this.enemies = new Enemies(this);
		this.spawner = new Spawner(this);

		this.keys = {
			W: this.input.keyboard!.addKey("W"),
			A: this.input.keyboard!.addKey("A"),
			S: this.input.keyboard!.addKey("S"),
			D: this.input.keyboard!.addKey("D"),
		};

		this.player!.animations.push(this.animations?.animations["yellow_slash"]!);
		this.player!.draw(this);

		this.physics.world.bounds.width = 5000;
		this.physics.world.bounds.height = 5000;
		this.cameras.main.startFollow(this.player!.image!);

		this.input.on("pointerdown", (pointer: Phaser.Input.Pointer) =>
			handleClick(this, pointer)
		);

		this.spawner!.start();

		// debug rectangles
		this.add.rectangle(
			innerConfig.width / 2,
			innerConfig.height / 2,
			30,
			30,
			0x00ff00
		);

		this.console = new DebugConsole(this);
	}

	update(_time: number, _delta: number): void {
		this.player!.handleInput(this.keys!);
		this.enemies!.list.forEach((enemy) => {
			if (enemy.iframes > 0) {
				enemy.iframes--;
			}
		});
		this.enemies!.move(this.player!);
	}

	debug(message: string): void {
		new Debugger(this, message).handle();
	}

	returnDebug(message: string): void {
		this.console?.receive(message);
	}
}

function handleClick(context: Game, pointer: Phaser.Input.Pointer): void {
	let zombie = enemies.zombie(context);

	context.spawner!.spawnAt({ x: pointer.worldX, y: pointer.worldY }, zombie);
}

export { Game, Keys };

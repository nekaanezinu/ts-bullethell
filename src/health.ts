import { EventEmitter, Listener } from "./utils/event_emitter";

enum HealthEvents {
  CHANGE = "change",
  DEATH = "death",
}

class Health {
  private _max: number;
  private _current: number;
  private _emitter: EventEmitter = new EventEmitter();

  constructor(max: number, current?: number) {
    this._max = max;
    this._current = current || max;
  }

  public get max(): number {
    return this._max;
  }

  public get current(): number {
    return this._current;
  }

  addEvent(event: HealthEvents, listener: Listener): void {
    this._emitter.on(event, listener);
  }

  takeDamage(damage: number) {
    let tmpCurrent = this._current;

    this._current -= damage;
    this._current = Math.max(0, this._current);

    if (this._current !== tmpCurrent) {
      this._emitter.emit("change");
    }

    if (this._current === 0) {
      this._emitter.emit("death");
    }
  }

  heal(heal: number) {
    this._current += heal;
    this._current = Math.min(this._max, this._current);

    this._emitter.emit("change");
  }
}

export { Health, HealthEvents };

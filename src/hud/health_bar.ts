import { Game } from "../game";
import { Health } from "../health";

const RED = 0xff0000;
const GREEN = 0x00ff00;

const FULL_WIDTH = 80;
const FULL_HEIGHT = 5;

class HealthBar {
  game: Game;
  graphics: Phaser.GameObjects.Graphics;

  constructor(game: Game) {
    this.game = game;
    this.graphics = this.game.add.graphics();
  }

  draw(x: number, y: number, health: Health): void {
    let offsetX = x - 40;
    let offsetY = y - 45;

    this.graphics.clear();

    this.graphics.fillStyle(RED, 1);
    this.graphics.fillRect(offsetX, offsetY, FULL_WIDTH, FULL_HEIGHT);

    this.graphics.fillStyle(GREEN, 1);
    this.graphics.fillRect(
      offsetX,
      offsetY,
      this.greenWidth(health),
      FULL_HEIGHT
    );
  }

  greenWidth(health: Health): number {
    return (health.current / health.max) * FULL_WIDTH;
  }
}

export { HealthBar };

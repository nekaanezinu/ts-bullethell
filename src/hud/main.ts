import { Game } from "../game";
import { Health } from "../health";
import { Movable } from "../utils/movable";
import { HealthBar } from "./health_bar";

class Hud {
  healthBar: HealthBar;
  game: Game;

  constructor(game: Game) {
    this.healthBar = new HealthBar(game);
    this.game = game;
  }
}

export { Hud };

import * as Phaser from "phaser";
import { Game } from "./game";
import { config } from "./config";

config.scene = Game;

const game: Phaser.Game = new Phaser.Game(config);

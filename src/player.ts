import { Game, Keys } from "./game";
import { Character } from "./character";
import { INITIAL_X, INITIAL_Y, Movable } from "./utils/movable";
import { InnerAnimation } from "./animation";
import { HealthEvents, Health } from "./health";
import { Enemy } from "./enemy";
import { Hud } from "./hud/main";
import { Levelable } from "./utils/levelable";

class Player {
  character: Character;
  image: Phaser.Types.Physics.Arcade.ImageWithDynamicBody | undefined;
  animations: InnerAnimation[] = [];
  health: Health = new Health(100);
  movable: Movable | undefined;
  levelable: Levelable;
  private hud: Hud;

  constructor(character: Character, hud: Hud) {
    this.character = character;
    this.hud = hud;
    this.levelable = new Levelable();
    this.health.addEvent(HealthEvents.CHANGE, () => {
      console.log(`player health: ${this.health.current}`);
    });
    this.health.addEvent(HealthEvents.DEATH, () => {
      console.log("player death");
    });
  }

  draw(context: Game): Phaser.GameObjects.Image {
    if (this.image) {
      this.image.destroy();
    }

    this.image = context.physics.add.image(
      INITIAL_X,
      INITIAL_Y,
      this.character.base_image
    );
    this.image.setPushable(false);
    this.image.setInteractive();
    this.movable = new Movable(this.image);

    return this.image;
  }

  handleInput(keys: Keys) {
    this.movable!.handleKeyboardInput(this, keys);
    if (keys.A.isDown) {
      this.image!.flipX = true;
    } else if (keys.D.isDown) {
      this.image!.flipX = false;
    }

    this.animations.forEach((anim) => {
      if (!anim.invertable) {
        return;
      }

      if (keys.A.isDown) {
        if (!anim.sprite!.flipX && !anim.active) {
          anim.sprite!.setPosition(
            anim.sprite!.x - anim.offsetX * 2,
            anim.sprite!.y
          );
          anim.sprite!.flipX = true;
        }
      } else if (keys.D.isDown) {
        if (anim.sprite!.flipX && !anim.active) {
          anim.sprite!.setPosition(
            anim.sprite!.x + anim.offsetX * 2,
            anim.sprite!.y
          );
          anim.sprite!.flipX = false;
        }
      }
    });
    this.hud.healthBar.draw(this.movable!.x, this.movable!.y, this.health);
  }

  addEnemy(context: Game, enemy: Enemy): void {
    context.physics.add.collider(this.image!, enemy.image!, (a, b) => {
      this.health.takeDamage(enemy.damage);
    });
  }
}

export { Player };

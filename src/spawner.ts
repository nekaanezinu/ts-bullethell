import * as Phaser from "phaser";

import { Game } from "./game";
import { Enemy, enemies } from "./enemy";
import { HealthEvents } from "./health";
import { Coordinates } from "./utils/coordinates";

const schedule: ScheduleItem[] = [
  { time: 1, count: 5, enemy: enemies.zombie },
  { time: 5000, count: 10, enemy: enemies.zombie },
  { time: 10000, count: 15, enemy: enemies.zombie },
];

type ScheduleItem = {
  time: number;
  count: number;
  enemy: (context: Game) => Enemy;
};

class Spawner {
  context: Game;
  cameraBounds: Phaser.Geom.Rectangle;

  constructor(context: Game) {
    this.context = context;
    this.cameraBounds = this.context.cameras.main.getBounds();
  }

  start() {
    let camera = this.context.cameras.main;

    schedule.forEach((item) => {
      this.context.time.addEvent({
        delay: item.time,
        callback: () => this.spawnForSchedule(item),
        callbackScope: this,
      });
    });
  }

  spawnForSchedule(item: ScheduleItem) {
    console.log(`spawning ${item.count} ${item.enemy(this.context).name}`);

    for (let index = 0; index < item.count; index++) {
      this.spawnAt(this.randCoords, item.enemy(this.context));
    }
  }

  spawnAt(coords: Coordinates, enemy: Enemy) {
    let image = enemy.draw(this.context, coords.x, coords.y);
    enemy.movable!.setPosition(coords.x, coords.y);
    this.context.player!.addEnemy(this.context, enemy);
  }

  get randCoords(): Coordinates {
    let side = Phaser.Math.Between(0, 3);
    let spawnX, spawnY;

    switch (side) {
      case 0: // Top
        spawnX = Phaser.Math.Between(
          this.cameraBounds.left,
          this.cameraBounds.right
        );
        spawnY = this.cameraBounds.top - 50;
        break;
      case 1: // Right
        spawnX = this.cameraBounds.right + 50;
        spawnY = Phaser.Math.Between(
          this.cameraBounds.top,
          this.cameraBounds.bottom
        );
        break;
      case 2: // Bottom
        spawnX = Phaser.Math.Between(
          this.cameraBounds.left,
          this.cameraBounds.right
        );
        spawnY = this.cameraBounds.bottom + 50;
        break;
      case 3: // Left
        spawnX = this.cameraBounds.left - 50;
        spawnY = Phaser.Math.Between(
          this.cameraBounds.top,
          this.cameraBounds.bottom
        );
        break;
    }

    return { x: spawnX!, y: spawnY! };
  }
}

export { Spawner };

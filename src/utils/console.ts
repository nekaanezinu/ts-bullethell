import { Game } from "../game";

class DebugConsole {
  context: Game;
  inputEl: HTMLInputElement;
  outputWrapperEl: HTMLDivElement;
  consoleEl: HTMLDivElement;

  constructor(context: Game) {
    this.context = context;
    this.inputEl = document.getElementById(
      "consoleInputText"
    ) as HTMLInputElement;

    this.outputWrapperEl = document.getElementById(
      "consoleDisplay"
    ) as HTMLDivElement;

    this.consoleEl = document.getElementById("devConsole") as HTMLDivElement;

    this.inputEl.addEventListener("focus", () => {
      this.context.input.keyboard!.removeAllKeys(true);
    });
    this.inputEl.addEventListener("blur", () => {
      this.context.keys!.W = this.context.input.keyboard!.addKey("W");
      this.context.keys!.A = this.context.input.keyboard!.addKey("A");
      this.context.keys!.S = this.context.input.keyboard!.addKey("S");
      this.context.keys!.D = this.context.input.keyboard!.addKey("D");
    });
    this.inputEl.addEventListener("keydown", (event: KeyboardEvent) => {
      if (event.key === "Enter") {
        this.send(this.inputEl.value);
        this.inputEl.value = "";
      }
      // does not register added keys for some UNHOLY reason
      if (event.key === "a") {
        this.inputEl.value += "a";
      }
      if (event.key === "s") {
        this.inputEl.value += "s";
      }
      if (event.key === "d") {
        this.inputEl.value += "d";
      }
      if (event.key === "w") {
        this.inputEl.value += "w";
      }
    });

    document.addEventListener("keydown", (event: KeyboardEvent) => {
      if (event.key === "`") {
        this.consoleEl.hidden = !this.consoleEl.hidden;
        if (!this.consoleEl.hidden) {
          this.inputEl.focus();
        }
        event.preventDefault();
      }
    });
  }

  send(message: string): void {
    if (message.length === 0) {
      return;
    }

    this.context.debug(message);
  }

  receive(message: string): void {
    this.outputWrapperEl.innerHTML += `<div class="outputItem">${message}</div>`;
    this.outputWrapperEl.scrollTop = this.outputWrapperEl.scrollHeight;
  }
}

export { DebugConsole };

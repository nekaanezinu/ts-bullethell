type Coordinates = {
  x: number;
  y: number;
};

export { Coordinates };

import { Game } from "../game";

class Debugger {
  context: Game;
  message: string;
  parts: string[];
  commandFound: boolean = true;

  constructor(context: Game, message: string) {
    this.context = context;
    this.message = message;
    this.parts = message.split(" ");
  }

  handle(): void {
    if (this.parts[0] === "physics_debug") {
      const debug = this.context.physics.world.drawDebug;
      const graphics = this.context.physics.world.debugGraphic;

      if (this.parts[1] === "true") {
        graphics.visible = true;
        this.context.returnDebug("Physics debug enabled");
        return;
      } else if (this.parts[1] === "false") {
        graphics.visible = false;
        this.context.returnDebug("Physics debug disabled");
        return;
      } else {
        this.commandFound = false;
      }
    }

    this.commandFound = false;
    if (!this.commandFound) {
      this.context.returnDebug("unrecognized command: " + this.parts[0]);
    }
  }
}

export { Debugger };

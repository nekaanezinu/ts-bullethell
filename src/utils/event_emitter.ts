type Listener = () => void;

class EventEmitter {
  private listeners: { [key: string]: Listener } = {};

  public on(event: string, listener: Listener): void {
    this.listeners[event] = listener;
  }

  public emit(event: string): void {
    if (this.listeners[event]) {
      this.listeners[event]();
    }
  }
}

export { EventEmitter, Listener };

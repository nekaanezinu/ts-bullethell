import { Game } from "../game";

const XP_SCALE = 1.5;
const BASE_XP = 5;

class Levelable {
  xp: number = 0;
  level: number = 1;

  addXp(xp: number): void {
    this.xp += xp;

    console.log(`xp: ${this.xp} / ${BASE_XP * this.level * XP_SCALE}`);

    if (this.xp >= BASE_XP * this.level * XP_SCALE) {
      this.level += 1;
      this.xp = 0;
    }

    console.log(`level up: ${this.level}`);
  }
}

export { Levelable };

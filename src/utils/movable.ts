import * as Phaser from "phaser";

import { Keys } from "../game";
import { innerConfig } from "../config";
import { Player } from "../player";

const INITIAL_X = innerConfig.width / 2;
const INITIAL_Y = innerConfig.height / 2;
const SPEED = 100;
const ENEMY_SPEED = 80;
const KNOCKBACK_SPEED = 300;

type NumericDirections = {
  x: number;
  y: number;
};

class Movable {
  image: Phaser.Types.Physics.Arcade.ImageWithDynamicBody;
  private _blockedFrames: number = 0;
  private _knockback: boolean = false;

  constructor(image: Phaser.Types.Physics.Arcade.ImageWithDynamicBody) {
    this.image = image;
  }

  get x(): number {
    return this.image.x;
  }

  get y(): number {
    return this.image.y;
  }

  setPosition(x: number, y: number) {
    this.image.setPosition(x, y);
  }

  knockback() {
    this._blockedFrames = 60;
    this._knockback = true;
  }

  moveAwayFrom(movable: Movable) {
    if (this._knockback) {
      this._blockedFrames -= 1;
    } else {
      return;
    }

    if (this._blockedFrames <= 0) {
      this._knockback = false;
      return;
    }

    let directions = this.directions(movable.image!.x, movable.image!.y, true);
    this.image!.setVelocity(
      directions!.x * KNOCKBACK_SPEED,
      directions!.y * KNOCKBACK_SPEED
    );
  }

  moveTowardsMovable(movable: Movable) {
    if (this._blockedFrames > 0) {
      this._blockedFrames--;
      return;
    }

    let directions = this.directions(movable.image!.x, movable.image!.y);
    this.image!.setVelocity(
      directions!.x * ENEMY_SPEED,
      directions!.y * ENEMY_SPEED
    );
  }

  directions(x: number, y: number, away: boolean = false): NumericDirections {
    let direction = new Phaser.Math.Vector2(x - this.x, y - this.y);
    direction.normalize();

    let distance = Phaser.Math.Distance.Between(this.x, this.y, x, y);

    if (distance < 1) {
      return { x: 0, y: 0 };
    }

    if (away) {
      return { x: -direction.x, y: -direction.y };
    }

    return { x: direction.x, y: direction.y };
  }

  handleKeyboardInput(player: Player, keys: Keys) {
    player.image!.setVelocity(0, 0);

    let velocityY = 0;
    let velocityX = 0;

    if (keys.W.isDown) {
      velocityY = -SPEED;
    }
    if (keys.S.isDown) {
      velocityY = SPEED;
    }
    if (keys.A.isDown) {
      velocityX = -SPEED;
    }
    if (keys.D.isDown) {
      velocityX = SPEED;
    }

    player.image!.setVelocity(velocityX, velocityY);
    player.animations.forEach((anim) =>
      anim.sprite!.setVelocity(velocityX, velocityY)
    );
  }
}

export { Movable, INITIAL_X, INITIAL_Y };
